/**
 * Created by Disk on 25.05.17.
 */

var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var del = require('del');
var runSequence = require('run-sequence');
const zip = require('gulp-zip');


gulp.task('default',function(){
   browserSync.init({
       server:{
           baseDir: 'src'
       }
   })
    gulp.watch('src/**',browserSync.reload);

});

gulp.task('zip', function(){
    return gulp.src('src/**')
        .pipe(zip('arch'+new Date().toISOString().replace('-','').replace('-','').replace(':','').replace(':','').replace('.','')+'.zip'))
        .pipe(gulp.dest('target'));
});

gulp.task('clean:target', function(){
    return del.sync('target');
})

gulp.task('build',function(callback){
    runSequence('clean:target','zip',callback);
})