function Game(countWidth, countHeight, margin, seconds, errorSeconds, level, countLevelColor, gameBlock, blockTime, blockError, blockTotal, callbackEvent, callback){
  this.countWidth = countWidth; // количество блоков по ширине
  this.countHeight = countHeight; // количество блоков по высоте
  this.margin = margin; // отступ между элементами
  this.countLevelColor = countLevelColor; // количество пунктов цвета
  //this.countLevelSize = 3; // количество уровней с размером сетки
  this.seconds = seconds; // количество секунд которое дается для правильного ответа
  this.errorSeconds = errorSeconds; // количество секунд отнимаемое от остатка времени если неправильно ответил 
  this.level = level;
  this.timer = null;

  this.gameBlock = gameBlock;
  this.time = blockTime;
  //this.error = blockError;
  this.total = blockTotal;

  this.currentErrors = 0;
  this.totalTime = seconds*100;
  this.totalTimeResult = null;

  this.start = function(){
    this.time.text(this.seconds + ':00');

    //this.error.text(this.currentErrors);

    this.timer = setInterval(function(object){       
      temp_totalTime = (object.totalTime/100).toFixed(2);
      if(temp_totalTime<10)
        temp_totalTime = '0' + temp_totalTime;
      object.time.text(temp_totalTime.replace(".", ":"));
      object.totalTime -= 1;
      if(object.totalTime < 0){
        clearInterval(object.timer);
        object.time.text('00:00');
        $($(object.gameBlock).find('div')).off(); // убираем бинд кликов на блоках
        object.printResult('time'); // вывод сообщения в таблицу логов о том что вышло время
        //alert('GAME OVER');		
      }
    }, 10, this);

    var widthBlock = (this.gameBlock.width()-(margin*(countWidth-1)))/countWidth;
    var heightBlock = (this.gameBlock.height()-(margin*(countHeight-1)))/countHeight;
    var allItems = countWidth*countHeight;
    var randItem = this.rand(0, allItems-1);
    var randColor = [this.rand(25, 220), this.rand(25, 220), this.rand(25, 220)];
    var randColorString = "rgb(" + randColor.toString() + ")";
    var activeColor = "rgb(" + this.randActiveColor(randColor).toString() + ")";
    this.randActiveColor(randColor);

    for(var i=0; i<allItems; i++){
      var block = $("<div/>");
      block.width(widthBlock);
      block.height(heightBlock);

      if(i == randItem){ // рандом
        block.addClass("active");

        block.css("background-color", activeColor);
      } else {
        block.css("background-color", randColorString);
      }

      if(i%countWidth != countWidth-1){ // установка правого отступа у блоков кроме последнего в ряду
        block.css("margin-right", margin);
      }

      if(allItems-countWidth > i){ // установка нижнего отступа у блоков кроме последнего ряда
        block.css("margin-bottom", margin);
      }	  

      block.bind('click', this, this.event );

      this.gameBlock.append(block);
    }
  }

  this.rand = function(min, max) {
    return Math.floor(arguments.length > 1 ? (max - min + 1) * Math.random() + min : (min + 1) * Math.random());
  }

  this.message = function(status, message, textSize, timeShow) {
    var blockMessage = $('<div class="block-message"/>'); // создаем блок сообщения
    if(status == 'good'){
      blockMessage.addClass('good-message');
    } else if(status == 'error'){
      blockMessage.addClass('error-message');
    } else {
      //
    }

    blockMessage.text(message); // добавляем в него текст сообщения
    blockMessage.css({"font-size": textSize}); // устанавливаем размер текста в блоке

    this.gameBlock.append(blockMessage);
    var posLeft = (blockMessage.width()/2)*-1;
    var posTop = (blockMessage.height()/2)*-1;

    blockMessage.css({"top": "50%", "left": "50%", "margin-left": posLeft, "margin-top": posTop}); // выравниваем по центру игрового поля

    blockMessage.show();
    setTimeout(function(){ // таймер через какое время скрыть сообщение и удалить объект сообщения
      blockMessage.hide();
      blockMessage.remove();
    }, timeShow);

  }

  this.event = function(object) {
    if($(this).attr('class')){
      clearInterval(object.data.timer);
      $($(object.data.gameBlock).find('div')).off();
      object.data.totalTimeResult = (((object.data.seconds*100) - object.data.totalTime - 1)/100);
      object.data.printResult('good');
      //object.data.message('good', 'ПРАВИЛЬНО', 30, 2000); // статус сообщения, текст сообщения, размер текста(пх), длительность показа сообещния
    } else {
      object.data.currentErrors++;
      //object.data.error.text(object.data.currentErrors);
      object.data.totalTime -= object.data.errorSeconds*100;
      object.data.message('error', '-2', 50, 50); // статус сообщения, текст сообщения, размер текста(пх), длительность показа сообещния
	  callbackEvent();
    }
  }

  this.printResult = function(status){
    if(status == 'time'){
      this.total.append('<p class="time_game">' + this.level + ' уровень не пройден, не хватило времени и допущено ' + this.currentErrors + ' ошибок.</p>');
      callback(false);
    } else if(status == 'good'){
      this.total.append('<p class="good_game">' + this.level + ' уровень пройден за ' + this.totalTimeResult + ' с. и допущено ' + this.currentErrors + ' ошибок.</p>');
      callback(true);
    } else {
      this.total.append('<p class="other_game">' + status + ' </p>');
    }
  }

  this.randActiveColor = function(arrColor){
	if(this.rand(0,1)){
		return [arrColor[0]-this.countLevelColor, arrColor[1]-this.countLevelColor, arrColor[2]-this.countLevelColor];
	} else {
		return [arrColor[0]+this.countLevelColor, arrColor[1]+this.countLevelColor, arrColor[2]+this.countLevelColor];
	}
    /*var colorR = (arrColor[0]+this.countLevelColor>255)?arrColor[0]-this.countLevelColor:arrColor[0]+this.countLevelColor;
    var colorG = (arrColor[1]+this.countLevelColor>255)?arrColor[1]-this.countLevelColor:arrColor[1]+this.countLevelColor;
    var colorB = (arrColor[2]+this.countLevelColor>255)?arrColor[2]-this.countLevelColor:arrColor[2]+this.countLevelColor;
    return [colorR, colorG, colorB];*/
  }
}

$(document).ready(function(){

  var margin = 3; // расстояние в пикселях между элементами/блоками

  /* START КОНФИГУРАЦИЯ СЛОЖНОСТИ */
  /* уменьшение времени */
  var levelDiffTime = 5; // каждые "levelDiffTime" уровней уменьшать время, данное на правильный ответ, на "levelReduceTime" секунд.
  var levelReduceTime = 1; // на сколько секунд уменьшать время данное для правильного ответа каждые "levelDiffTime" уровней.
  /* уменьшение времени */

  /* увеличение количества блоков по ширине */
  var levelDiffBlockWH = 10; // каждые "levelDiffBlockW" уровней увеличивать количество блоков по ширине.
  var levelReduceBlockWH = 1; // на сколько блоков увеличивать по ширине и высоте каждые "levelDiffBlockWH" уровней.
  /* увеличение количества блоков по ширине */
  
  /* уменьшение количества пунктов цвета */
  var levelDiffColor = 3; // каждые "levelDiffColor" уровней уменьшать количество пунктов цвета.
  var levelReduceColor = 1; // на сколько пунктов уменьшать тон цвета каждые "levelDiffColor" уровней.
  /* уменьшение количества пунктов цвета */
	/* END КОНФИГУРАЦИЯ СЛОЖНОСТИ */
  
  var levelsMessages = {
	'0' : ["Тренируйся больше! Попробуй ещё раз!","Materials/image_start.png"], // этот пункт, нулевой, не удалять
	'5' : ["Ты хорошо отличаешь оттенки! Но, посмотри, как это делает принтер HP DeskJet GT!","Materials/image_5-10.png"],
	'10' : ["Ты идеально отличаешь оттенки! Как новый принтер HP DeskJet GT","Materials/image_10>.png"]
  };
  
  
  var startButton = $('#start');
  var restartButton = $('#restart');
  var gameBlock = $('#game_block');
  var timeBlock = $('#time');
  var errorBlock = $('#error>span');
  var totalBlock = $('#total');
  var levelMessage = $('#level>span');
  var resultMessage = $('#result_message');
  var resultPoints = $('#result_points>span');
  var resultImage = $('#result_image');
  var blockInfo = $('#block_info');
  var resultsBlock = $("#results")
  var snd = $('#snd');

  /* стартовые параметры, нужны именно для первого уровня, далее будут менятся в зависимости от сложности */  
  var currentLevel = 1;  // текущий уровен, по умолчанию первый
  var temp_second = 23; // количество секунд которое дается для правильного ответа
  var temp_blockW = 4; // количество блоков по ширине
  var temp_blockH = 4; // количество блоков по высоте
  var temp_minusSecond = 2; // количество секунд отнимаемое от остатка времени если неправильно ответил
  var temp_punktColor = 30; // количество пунктов цвета

	
  /* ВСЕ ЧТО НИЖЕ, НЕ ТРОГАТЬ */
  var game = null;
  var allErrors = 0;
  
  var t_sec = temp_second, t_blocW = temp_blockW, t_blockH = temp_blockH, t_minus = temp_minusSecond, t_color = temp_punktColor;
  
  levelMessage.text(currentLevel);
  errorBlock.text(allErrors);
  
  startButton.click(function(){
    /*
		1 - количество блоков по ширине
		2 - количество блоков по высоте
		3 - отступ между элементами
		4 - количество секунд которое дается для правильного ответа
		5 - количество секунд отнимаемое от остатка времени если неправильно ответил
		6 - текущий уровень
		7 - количество пунктов цвета
	 */
    v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
      event:  EVENT_ACTIVE,
      params: {answers: 'Start_Game'}
    });
    errorBlock.text(allErrors);
	levelMessage.text(currentLevel);
    resultMessage.empty();
    startGame(temp_blockW, temp_blockH, margin, temp_second, temp_minusSecond, currentLevel, temp_punktColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback);
    startButton.hide(); // скрываем кнопку
    //gameBlock.empty();
    //game = new Game(4, 4, 3, 15, 2, 1, 15, gameBlock, timeBlock, errorBlock, totalBlock, callback); // инициализация нового уровня
    //game.start();
    //startButton.prop('disabled', true); // диактивируем кнопку СТАРТ чтобы игрок не смог сбрасить игру
    snd[0].src = "hp_music.mp3";
  });

  startButton.mouseover(function(){
    stbTw.play(0);
  })


  startButton.mouseout(function(){
    stbTw.pause(0);
  })

  var stbTw = TweenMax.to("#start-bar",2,{paused: true,width:"100%",onComplete:function(){
    v5("#block_info").style.display = "block";
    v5("#img_start").style.display = "none";
    v5("#tutorial_text").style.display = "none";
    v5("#start-pb").style.display = "none";
    v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
      event:  EVENT_ACTIVE,
      params: {answers: 'Hover_Start_Game'}
    });
    errorBlock.text(allErrors);
    levelMessage.text(currentLevel);
    resultMessage.empty();
    startGame(temp_blockW, temp_blockH, margin, temp_second, temp_minusSecond, currentLevel, temp_punktColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback);
    startButton.hide();
    snd[0].src = "hp_music.mp3";
  }.bind(this)})

  restartButton.click(function(e){
    e.preventDefault();
    e.stopPropagation();
    currentLevel = 1;
    errorBlock.text(allErrors);
    levelMessage.text(currentLevel);
    resultMessage.empty();
    resultsBlock.hide();
    blockInfo.show();
    v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
      event:  EVENT_ACTIVE,
      params: {answers: 'Click_Restart_Game'}
    });
    startGame(temp_blockW, temp_blockH, margin, temp_second, temp_minusSecond, currentLevel, temp_punktColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback);
    snd[0].src = "hp_music.mp3";
  });

  var startGame = function(countWidth, countHeight, margin, seconds, errorSeconds, level, countLevelColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback){
    gameBlock.empty();
    game = new Game(countWidth, countHeight, margin, seconds, errorSeconds, level, countLevelColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback);
    game.start();
  }

  function callbackEvent(){
	allErrors += 1;
    errorBlock.text(allErrors);
  }


	function getMessageGameOver(){
		var last_message = levelsMessages['0'];
		for (var message in levelsMessages) {
			if(currentLevel >= message){
              last_message = levelsMessages[message][0];
              resultImage[0].src = levelsMessages[message][1];
              if(levelsMessages[message][1] == "Materials/image_start.png"){
                resultImage[0].style.width = "308px";
                $("#buttons")[0].style.marginTop = "30px";
                resultImage[0].style.marginLeft = "10px";
                $("#sh_vk")[0].style.marginRight = "-40px";
              } else if(levelsMessages[message][1] == "Materials/image_5-10.png"){
                $("#imgs")[0].style.marginTop = "15px";
                resultImage[0].style.width = "324px";
                $("#buttons")[0].style.marginTop = "35px";
                resultImage[0].style.marginLeft = "0px";
                $("#sh_vk")[0].style.marginRight = "-30px";
              } else if(levelsMessages[message][1] == "Materials/image_10>.png"){
                $("#imgs")[0].style.marginTop = "-10px";
                resultImage[0].style.width = "342px";
                $("#buttons")[0].style.marginTop = "30px";
                resultImage[0].style.marginLeft = "0px";
                $("#sh_vk")[0].style.marginRight = "-20px";
              }
            }
		}
		return last_message;
	}
  
  
  function callback(flag){
    if(!flag){
      console.log(currentLevel);
        if(currentLevel < 6){
          v5("#lmb").style.display = "none";
          v5("#restart").style.display = "block";
        } else {
          v5("#lmb").style.display = "block";
          v5("#restart").style.display = "none";
        }
        snd[0].src = "";
        snd[0].load();
		// конец игры, подведение итогов
		currentLevel--;
		levelMessage.text(currentLevel);
		//startButton.show();
		resultMessage.text(getMessageGameOver());
		resultPoints.text(currentLevel);
        blockInfo.hide();
        resultsBlock.show();
		temp_second = t_sec;
		temp_blockW = t_blocW;
		temp_blockH = t_blockH;
		temp_minusSecond = t_minus;
		temp_punktColor = t_color;
		allErrors = 0;
		return;
	}
    
    if(currentLevel % levelDiffTime == 0){
      temp_second -= levelReduceTime; // уменьшаем время для ответа
      game.printResult('Уменьшено время для ответа до ' + temp_second + ' секунд.');
    }

    if(currentLevel % levelDiffBlockWH == 0){
      temp_blockW += levelReduceBlockWH; // увеличиваем количество блоков по ширине
      temp_blockH += levelReduceBlockWH; // увеличиваем количество блоков по высоте
      game.printResult('Увеличено количество блоков по ширине и высоте до ' + temp_blockW);
    }
    
    if(currentLevel % levelDiffColor == 0){
      temp_punktColor -= levelReduceColor; // уменьшаем количество пунктов цвета
      game.printResult('Уменьшено количество пунктов цвета до ' + temp_punktColor);
    }

    /*if(temp_blockW <= 1){
      game.printResult('Вы стали победителем и дошли до ' + currentLevel + ' уровня.');
      game.message('good', 'WIN!!!', 50, 5000); // статус сообщения, текст сообщения, размер текста(пх), длительность показа сообещния
      return;
    }*/

    currentLevel++;
    levelMessage.text(currentLevel);
    startGame(temp_blockW, temp_blockH, margin, temp_second, temp_minusSecond, currentLevel, temp_punktColor, gameBlock, timeBlock, errorBlock, totalBlock, callbackEvent, callback); 
  }

});
