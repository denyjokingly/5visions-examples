from PIL import Image, ImageDraw, ImageFont
import sys,os
 
def main():
    
    base = os.getcwd()
    #print(os.listdir(base+"/Fonts.001"))
    vk_file = base+"/Materials/vk_banner.png"
    ok_file = base+"/Materials/ok_banner.png"
    f_file = base+"/Fonts.001/HPSimplified_BD.ttf"
    txt = ''
    
    o_file = ''
    points = 0
    

    for i in range(0,51):
        o_file = 'ok_images/'+str(i)+'.png'
        print(o_file)
        if i in [1,21,31,41]:
            txt = "ОТТЕНОК"
        elif i in [2,3,4,22,23,24,32,33,34,42,43,44]:
            txt = "ОТТЕНКA"
        else:
            txt = "ОТТЕНКОВ"               

        with Image.open(open(ok_file, 'rb')).convert('RGBA') as im:
            d = ImageDraw.Draw(im)
            f = ImageFont.truetype(f_file,38)
            x = 19
            y = 105
            d.text((x,y), str(i) + " " + txt, font=f,fill=(0,150,214,255))
            im.save(o_file)

            
    


if __name__ == "__main__" : main()
